﻿using System;
using System.Collections.ObjectModel;

namespace Core.ViewModel
{
    public class WorldViewModel
    {

        public WorldViewModel()
        {
            characters = new ObservableCollection<ICharacter>()
            {
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},
                new CharacterViewModel() {Name = "Asd", Uid = Guid.NewGuid()},

            };

        }

        private ObservableCollection<ICharacter> characters;
        public ObservableCollection<ICharacter> Characters
        {
            get
            {
                return characters;
            }
        }

        public readonly ObservableCollection<IObject> Objects = new ObservableCollection<IObject>();
        
    }
}
