﻿using System;

namespace Core.ViewModel
{
    public class CharacterViewModel : ICharacter
    {
        private readonly Character character = new Character();
        public Guid Uid
        {
            set { character.Uid = value; }
            get { return character.Uid; }
        }

        public string Name
        {
            set { character.Name = value; }
            get { return character.Name; }
        }
    }
}
