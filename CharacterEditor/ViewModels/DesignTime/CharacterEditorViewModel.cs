﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.DesignTime;
using Core.ViewModel.DesignTime;
using System.Collections.ObjectModel;
using Core;

namespace CharacterEditor.ViewModels.DesignTime
{
    class CharacterEditorViewModel// : ViewModels.CharacterEditorViewModel
    {
        public WorldViewModel WorldViewModel  { get { return new WorldViewModel(); }}


        public CharacterEditorViewModel()
        {
            characters = new ObservableCollection<ICharacter>()
            {
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),
                new CharacterViewModel(),

            };

        }

        private ObservableCollection<ICharacter> characters;
        public ObservableCollection<ICharacter> Characters
        {
            get
            {
                return characters;
            }
        }

        public readonly ObservableCollection<IObject> Objects = new ObservableCollection<IObject>();
        
    }
    
}
