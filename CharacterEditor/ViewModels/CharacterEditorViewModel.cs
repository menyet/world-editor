﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Remoting.Messaging;
using System.Windows.Input;
using Core;
using Core.ViewModel;
using Shared;
using ICharacter = Core.ICharacter;

namespace CharacterEditor.ViewModels
{
    public class CharacterEditorViewModel : ITitledDataContext
    {
        private readonly WorldViewModel world;


        public string Title { get { return "Character editor"; } }


        public CharacterEditorViewModel(WorldViewModel world)
        {
            this.world = world;
            NewCharacterCommand = new RelayCommand((param) => NewCharacter());
            RemoveCharacterCommand = new RelayCommand<ICharacter>(RemoveCharacter);
        }

        private void RemoveCharacter(ICharacter character)
        {
            throw new NotImplementedException();
        }

        private void NewCharacter()
        {

            

           
            Characters.Add(new CharacterViewModel() { Uid = Guid.NewGuid(), Name = "Brutus" });


        }



        public ObservableCollection<ICharacter> Characters { get { return WorldViewModel.Characters; } }



        public WorldViewModel WorldViewModel
        {
            get { return world; }
        }

        public ICommand NewCharacterCommand { get; set; }
        public ICommand RemoveCharacterCommand { get; set; }


    }
}
