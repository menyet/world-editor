﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace CharacterEditor
{
    class CharacterEditorModule : IModule
    {
        

        public string Name { get { return "Characters"; } }

        IEnumerable<ToolInfo> IModule.GetToolsInfo()
        {
            return new List<ToolInfo>
            {
                new ToolInfo { UserControlType = typeof(CharacterEditor), ViewModelType = typeof(ViewModels.CharacterEditorViewModel)},
                new ToolInfo { UserControlType = typeof(CharacterEditor), ViewModelType = typeof(ViewModels.CharacterEditorViewModel)},
                new ToolInfo { UserControlType = typeof(CharacterEditor), ViewModelType = typeof(ViewModels.CharacterEditorViewModel)},
                new ToolInfo { UserControlType = typeof(CharacterEditor), ViewModelType = typeof(ViewModels.CharacterEditorViewModel)},
                new ToolInfo { UserControlType = typeof(CharacterEditor), ViewModelType = typeof(ViewModels.CharacterEditorViewModel)},
                new ToolInfo { UserControlType = typeof(CharacterEditor), ViewModelType = typeof(ViewModels.CharacterEditorViewModel)},
            };
        }
    }
}
