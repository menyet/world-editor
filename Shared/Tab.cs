﻿using System.Windows;

namespace Shared
{
    public class Tab
    {
        public string Title { get { return DataContext.Title; } }

        private ITitledDataContext DataContext { get; set; }

        public FrameworkElement Content { get; protected set; }


        public Tab(ITitledDataContext dataContext, FrameworkElement content)
        {
            DataContext = dataContext;
            Content = content;

            Content.DataContext = DataContext;
        }
    }
}