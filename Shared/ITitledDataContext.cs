﻿namespace Shared
{
    public interface ITitledDataContext
    {
        string Title { get; }
    }
}