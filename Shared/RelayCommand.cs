﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Shared
{
    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> action;


        public RelayCommand(Action<T> action)
        {
            this.action = action;
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            action((T)parameter);
        }

        public event EventHandler CanExecuteChanged;
    }

    public class RelayCommand : RelayCommand<object>
    {
        public RelayCommand(Action<object> action) : base(action)
        {
        }
    }

}
