﻿using System.Collections.ObjectModel;

namespace Core.ViewModel.DesignTime
{
    public class WorldViewModel
    {
        private readonly ObservableCollection<ICharacter> characters = new ObservableCollection<ICharacter>()
        {
            new CharacterViewModel()
            {
                Name = "Johnny Cage",
            }
        };

        public ObservableCollection<ICharacter> Characters
        {
            get { return characters; }
        }



        public readonly ObservableCollection<IObject> Objects = new ObservableCollection<IObject>();


    }
}
