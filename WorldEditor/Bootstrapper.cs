﻿using System.Linq;
using Core;
using Ninject;
using Ninject.Syntax;

namespace WorldEditor
{
    public class Bootstrapper
    {
        public IKernel Kernel;


        public Bootstrapper()
        {
            Kernel = new StandardKernel();

            Kernel.Bind<Core.ViewModel.WorldViewModel>().To<Core.ViewModel.WorldViewModel>();

            LoadDynamicModules();
        }

        public void LoadDynamicModules()
        {
            var assembly = System.Reflection.Assembly.LoadFrom("CharacterEditor.dll");
            foreach (var type in assembly.GetTypes())
            {
                if (type.GetInterface("IModule") != null)
                {
                    var module = Kernel.Get(type) as IModule;

                    Kernel.Bind<IModule>().To(type);
                }
            }
            
        }


    }
}