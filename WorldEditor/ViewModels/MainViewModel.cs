﻿using System.Collections.Generic;
using System.Windows.Controls;
using Core;
using Ninject;
using Ninject.Syntax;
using Shared;

namespace WorldEditor.ViewModels
{


    class MainViewModel
    {

        private readonly IResolutionRoot ioC;



        //[Inject]
        private List<IModule> Modules { get; set; }



        [Inject] 
        public World World { get; set; }



        public List<Tab> Tabs { get; set; }

        public MainViewModel(IResolutionRoot IoC, List<IModule> Modules)
        {
            ioC = IoC;


            this.Modules = new List<IModule>();
            this.Modules.AddRange(Modules);


            Tabs = new List<Tab>();

            foreach (var module in Modules)
            {
                foreach (var tool in module.GetToolsInfo())
                {
                    var userControl = IoC.Get(tool.UserControlType) as UserControl;
                    var viewModel = IoC.Get(tool.ViewModelType) as ITitledDataContext;



                    Tabs.Add(new Tab(viewModel, userControl));
                    //Tabs.Add(new Tab());
                }
            }


            


            /*var i = IoC.Get<CharacterEditor.CharacterEditor>();

            var j = IoC.Get<CharacterEditor.ViewModels.CharacterEditorViewModel>();

            
            

            Modules.Add(IoC.Get<CharacterEditor.CharacterEditor>());
            Modules.Add(IoC.Get<CharacterEditor.CharacterEditor>());*/

        }
        
        
        

        
    }
}
