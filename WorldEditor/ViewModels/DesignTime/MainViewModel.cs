﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Core;
using Ninject;
using Ninject.Syntax;
using Shared;

namespace WorldEditor.ViewModels.DesignTime
{


    class MainViewModel
    {




        //private List<IModule> Modules { get; set; }



        //public World World { get; set; }



        public List<Tab> Tabs
        {
            get
            {
                return new List<Tab>
                {
                    new Tab(new TitledViewModel(), new Button() { Margin = new Thickness(50, 50, 50, 50) }),
                    new Tab(new TitledViewModel(), new Button() { Margin = new Thickness(50, 50, 50, 50) }),
                    new Tab(new TitledViewModel(), new Button() { Margin = new Thickness(50, 50, 50, 50) }),
                };
            }
        }

        public MainViewModel()
        {
        }
        
        
        

        
    }

    internal class TitledViewModel : ITitledDataContext
    {
        public string Title {
            get { return "Title"; }
        }
    }
}
