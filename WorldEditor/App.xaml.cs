﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using Ninject;
using WorldEditor.ViewModels;

namespace WorldEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Bootstrapper bootstrapper;


        public App()
        {
            bootstrapper = new Bootstrapper();

            
        }

        private void App_OnStartup(object sender, StartupEventArgs e)
        {

            MainWindow = new MainWindow();


            var vm = bootstrapper.Kernel.Get<MainViewModel>();

            MainWindow.DataContext = vm;

            MainWindow.Show();
        }


    }
}
