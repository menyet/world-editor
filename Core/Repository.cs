﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
	class Repository<T> : IEnumerable<T>
	{

		private List<T> list;


		public void Add(T entity)
		{
			list.Add(entity);
		}

		public void Remove(T entity)
		{
			list.Remove(entity);
		}
		
		public IEnumerator<T> GetEnumerator()
		{
			return list.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return list.GetEnumerator();
		}

		internal void Save()
		{
		}
	}
}
