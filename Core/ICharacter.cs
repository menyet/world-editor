﻿using System;

namespace Core
{
    public interface ICharacter
    {
        Guid Uid { get; }
        string Name { get; set; }
    }
}