﻿using System;

namespace Core
{
    public class ToolInfo
    {

        public Type ViewModelType { get; set; }
        public Type UserControlType { get; set; }
    }
}