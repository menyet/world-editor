﻿using System;

namespace Core
{
    public class Character
    {
        public Guid Uid { get; set; }
        public string Name { get; set; }
    }
}