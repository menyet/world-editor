﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IModule
    {
        IEnumerable<ToolInfo> GetToolsInfo();

        string Name { get; }
    }
}
